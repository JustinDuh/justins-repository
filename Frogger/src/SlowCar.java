

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class SlowCar extends GameObject {

	public SlowCar(int x, int y, ID id) {
		super(x, y, id);
		// TODO Auto-generated constructor stub
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle(x,y,100,100);
	}
	
	public void tick() {
		VelocityX = -1;
		
		x += VelocityX;
		y += VelocityY;
		
		//x = Game.clamp(x,  0,  Game.HEIGHT - 32);
		//y = Game.clamp(y, 0, Game.WIDTH - 52);
		
		
	}


	public void render(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		g.setColor(Color.red);
		g.fillRect(x,y,100,100);
		
	}

}
