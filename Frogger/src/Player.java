

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;


public class Player extends GameObject {//implements ImageObserver{

	Handler handler;
	
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
	}

	public Rectangle getBounds()
	{
		return new Rectangle(x,y,32,32);
	}
	
	public void tick() {
		x += VelocityX;
		y += VelocityY;
		
		
		x = Game.clamp(x,  0,  Game.HEIGHT - 32);
		y = Game.clamp(y, 0, Game.WIDTH - 52);
		
		collision();
	}

	private void collision()
	{
		for(int i = 0; i < handler.object.size(); i++)
		{
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getID() == ID.SlowCar)
			{
				if(getBounds().intersects(tempObject.getBounds()))
				{
					HUD.HEALTH--;
				}
			}
		}
	}
	
	public void render(Graphics g) {
		//Image frog = new ImageIcon("images/pikachu.png").getImage();
		//Image img = Toolkit.getDefaultToolkit().getImage("pikachu.png");
		//GameObject tempObject;
		Graphics2D g2d = (Graphics2D) g;
		//GImage frog = new GImage("pikachu.png", tempObject.getX(), tempObject.getY(), 32,32);
		//g.drawImage(img, 32, 32, this);
		g.setColor(Color.white);
		g.fillRect(x,y,32,32);
		
	}

//	public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
