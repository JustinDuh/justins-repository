

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
//import com.game.main.Game.STATE;

public class Menu extends MouseAdapter{
	
	private Game game;
	private Handler handler;
	
	public Menu(Game game, Handler handler)
	{
		this.game = game;
		this.handler = handler;
	}

	public void mousePressed(MouseEvent e)
	{
		int mouseX = e.getX();
		int mouseY = e.getY();
		
		//Play!
		if(mouseOver(mouseX, mouseY, 100, 400, 250, 50))
		{
			handler.addObject(new Player(220, 588, ID.Player, handler));
			handler.addObject(new SlowCar(600, 200, ID.SlowCar));
			game.gameState = STATE.Game;
		}
		
		//HELP
		if(mouseOver(mouseX, mouseY, 100, 460, 250, 50))
		{
			game.gameState = STATE.Help;
//			handler.addObject(new Player(220, 588, ID.Player, handler));
//			handler.addObject(new SlowCar(180, 200, ID.SlowCar));
			
		}
		
		if(game.gameState == STATE.Help)
		{
			if(mouseOver(mouseX, mouseY, 50, 20, 100, 75 ))
			{
				game.gameState = STATE.Menu;
				return;
			}
		}
		
		//QUIT
		if(mouseOver(mouseX, mouseY, 100, 520, 250, 50))
		{
			game.gameState = STATE.Quit;
			System.exit(1);
		}
		
	}
	
	public void mouseReleased(MouseEvent e)
	{
		
	}
	
	private boolean mouseOver(int mouseX, int mouseY, int x, int y, int width, int height)
	{
		if(mouseX > x && mouseX < x + width)
		{
			if(mouseY > y && mouseY < y + height)
			{
				return true;
			} else return false;
		}else return false;
	}
	
	public void tick()
	{
		
	}
	
	public void render(Graphics g)
	{
//		BufferedImage img = null;
//		try {
//			img = ImageIO.read(new File("pikachu.png"));
//		} catch (IOException e) {
//		}
//		

		if(game.gameState == STATE.Menu)
		{
			//Fonts
			Font fnt = new Font("arial", 1, 30);
			g.setFont(fnt);
			
			//TITTLE
			g.setColor(Color.green);
			g.drawString("FROGGER", 180, 100);
			
			//Play! (button 1)
			g.setColor(Color.white);
			g.drawRect(100, 400, 250, 50);
			g.drawString("Play!", 180, 440);
			
//			//LEVELS???
//			g.setColor(Color.white);
//			g.drawRect(100, 400, 250, 50);
//			g.drawString("Levels", 153, 450);
			
			//HELP (button2)
			g.setColor(Color.white);
			g.drawRect(100, 460, 250, 50);
			g.drawString("HELP", 180, 500);
			
			//QUIT (button3)
			g.setColor(Color.white);
			g.drawRect(100, 520, 250, 50);
			g.drawString("QUIT", 180, 560);
		}

		
		else if(game.gameState == STATE.Help)
		{
			Font fnt = new Font("arial", 1, 30);
			g.setFont(fnt);
			g.setColor(Color.white);
			
			//BACK BUTTON for help
			g.drawRect(50, 20, 100, 75);
			g.drawString("Back", 50, 75);
			
			g.drawString("HELP", 50, 150);
			g.drawString("Use arrow keys to navigate ", 50, 250);
			g.drawString("through traffic ", 50, 375);
		}
	}
}
