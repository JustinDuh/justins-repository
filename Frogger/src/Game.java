
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

public class Game extends Canvas implements Runnable{
	
	private Thread thread;
	private boolean running = false;
	private Handler handler;
	private HUD hud;
	private Menu menu;

	//Serial ID
	private static final long serialVersionUID = 3180085066718631436L;
	//Default Window Size
	public static final int WIDTH = 800, HEIGHT = 600;

	public Game()
	{
		handler = new Handler();
		menu = new Menu(this,handler);
		this.addMouseListener(menu);
		this.addKeyListener(new KeyInput(handler));
		new Window(WIDTH,HEIGHT, "Frogger!", this);
	

		hud = new HUD();
		
//		if(gameState == STATE.Game)
//		{
//			handler.addObject(new Player(220, 588, ID.Player, handler));
//			handler.addObject(new SlowCar(180, 200, ID.SlowCar));
//			//handler.addObject(new Player(200,100,ID.Player));
//		}
	}
	public synchronized void start() 
	{
		thread = new Thread(this);
		thread.start();
		running = true;
	}

	public synchronized void stop()
	{
		try{
			thread.join();
			running = false;
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
 STATE gameState = STATE.Menu;
	
	private void tick()
	{
		if(gameState == gameState.Game)
		{
			handler.tick();
			hud.tick();
		}
		else if(gameState == gameState.Menu)
		{
			menu.tick();
		}
	}

    private void render()
    {
        BufferStrategy bs = this.getBufferStrategy();
        if(bs == null)
        {
        	this.createBufferStrategy(3);
            return;
        }      
        
        Graphics g = bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, HEIGHT, WIDTH);
        
        handler.render(g);
        
        if(gameState == gameState.Game)
        {
        	hud.render(g);
        }
		else if(gameState == gameState.Menu || gameState == gameState.Help)
		{
			menu.render(g);
		}
        
        g.dispose();
        bs.show();
    }

	 public void run()
	 {
         this.requestFocus();
         long lastTime = System.nanoTime();
         double amountOfTicks = 60.0;
         double ns = 1000000000 / amountOfTicks;
         double delta = 0;
         long timer = System.currentTimeMillis();
         int frames = 0;
         while(running){
                 long now = System.nanoTime();
                 delta += (now - lastTime) / ns;
                 lastTime = now;
                 while(delta >= 1){
                         tick();
                         delta--;
                 }
                 if(running)
                         render();
                 frames++;
                 if(System.currentTimeMillis() - timer > 1000){
                         timer += 1000;
                       //  System.out.println("FPS: " + frames);
                         frames = 0;
                 }
         }
         stop();
 }

	public static int clamp(int var, int min, int max)
	{
		if(var >= max)
		{
			return var = max;
		}
		else if(var <= min)
		{
			return var = min;
		}
		else return var;
	}
	 
	public static void main(String args[]) 
	{
		new Game();
	}

}
