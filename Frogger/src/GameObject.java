

import java.awt.Graphics;
import java.awt.Rectangle;

//import acm.program.GraphicsProgram;

public abstract class GameObject {//extends GraphicsProgram {

	protected int x,y;
	
	protected ID id;
	
	protected int VelocityX, VelocityY; //Velocity controls the speed
	
	public GameObject(int x, int y, ID id)
	{
		this.x = x;
		this.y = y;
		this.id = id;
	}

	public abstract void tick();
	public abstract void render(Graphics g);
	public abstract Rectangle getBounds();
	
	public void setX(int x)
	{
		this.x = x;
	}
	public void setY(int y)
	{
		this.y = y;
	}
	public void setID(ID id)
	{
		this.id = id;
	}
	public void setVelocityX(int VelocityX)
	{
		this.VelocityX = VelocityX;
	}
	public void setVelocityY(int VelocityY)
	{
		this.VelocityY = VelocityY;
	}
	public int getX()
	{ 
		return x;
	}
	public int getY()
	{
		return y;
	}
	public ID getID()
	{
		return id;
	}
	public int getVelocityX()
	{
		return VelocityX;
	}
	public int getVelocityY()
	{
		return VelocityY;
	}
}
