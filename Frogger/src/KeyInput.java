

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter{

	private Handler handler;
	
	
	public KeyInput(Handler handler)
	{
		this.handler = handler;
		
	}
	
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();
		
		System.out.println(key);
		
		for(int i = 0; i < handler.object.size(); i++)
		{
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getID() == ID.Player)
			{
				if(key == KeyEvent.VK_UP)
				{
					tempObject.setY(tempObject.getY() - 100);
				}
				if(key == KeyEvent.VK_DOWN)
				{
					tempObject.setY(tempObject.getY() + 100);
					
				}
				if(key == KeyEvent.VK_LEFT)
				{
					tempObject.setX(tempObject.getX() - 100);
					
				}
				if(key == KeyEvent.VK_RIGHT)
				{
					tempObject.setX(tempObject.getX() + 100);
			
				}
			}
		}
		
		if(key == KeyEvent.VK_ESCAPE)
		{
			//if Escape during game show mini menu 1)Sound 2)Change Level 3)Back 4) Exit
			//if on main menu exit
			System.exit(1);;
			//Fill in later
		}
	}
	
	public void keyReleased(KeyEvent e)
	{

	}
}

